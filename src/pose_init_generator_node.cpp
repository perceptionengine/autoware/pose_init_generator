#include <memory>
#include <string>


#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "geometry_msgs/msg/pose_with_covariance.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"

class PoseInitGeneratorNode : public rclcpp::Node
{
public:

  explicit PoseInitGeneratorNode(const rclcpp::NodeOptions & options)
  : Node("pose_init_generator", options)
  {
    std::string init_pose;
    init_pose = declare_parameter("initial_pose", "0, 0, 0, 0, 0, 0, 0");
    pose_frame_ = declare_parameter("frame", "viewer");

    initial_pose_ = ParsePose(init_pose);
    initial_pose_.header.frame_id = pose_frame_;

    sub_ = create_subscription<sensor_msgs::msg::PointCloud2>(
      "pointcloud_map", rclcpp::QoS(rclcpp::KeepLast(1)).best_effort().durability_volatile(),
      std::bind(&PoseInitGeneratorNode::onPointCloud, this, std::placeholders::_1));

    pose_pub_  = this->create_publisher<geometry_msgs::msg::PoseWithCovarianceStamped>("/initialpose", 1);
  }

private:

  std::string pose_frame_;
  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr sub_;
  rclcpp::Publisher<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr pose_pub_;
  geometry_msgs::msg::PoseWithCovarianceStamped initial_pose_;

  geometry_msgs::msg::PoseWithCovarianceStamped ParsePose(const std::string &in_pose) {
      std::vector<float> pose_vect;
      geometry_msgs::msg::PoseWithCovarianceStamped initial_pose;
      std::stringstream ss(in_pose);
      for (float i; ss >> i;) {
          pose_vect.push_back(i);
          if (ss.peek() == ',' || ss.peek() == ' ')
              ss.ignore();
      }
      if (pose_vect.size() != 7) {
          RCLCPP_ERROR(this->get_logger(), "Pose should be seven in size x y z qx qy qz qw. Using 0 for all elements");
      } else {
          initial_pose.pose.pose.position.x = pose_vect[0];
          initial_pose.pose.pose.position.y = pose_vect[1];
          initial_pose.pose.pose.position.z = pose_vect[2];

          initial_pose.pose.pose.orientation.x = pose_vect[3];
          initial_pose.pose.pose.orientation.y = pose_vect[4];
          initial_pose.pose.pose.orientation.z = pose_vect[5];
          initial_pose.pose.pose.orientation.w = pose_vect[6];
      }
      return initial_pose;
  }

  void onPointCloud(const sensor_msgs::msg::PointCloud2::ConstSharedPtr cloud_ros)
  {
      initial_pose_.header = cloud_ros->header;
      pose_pub_->publish(initial_pose_);
    RCLCPP_INFO_STREAM(
      get_logger(), "Map Received. Initializing pose on frame:" << pose_frame_
      << ", x:" << initial_pose_.pose.pose.position.x
        << ", y:" << initial_pose_.pose.pose.position.y
        << ", z:" << initial_pose_.pose.pose.position.z
        << ", qx:" << initial_pose_.pose.pose.orientation.x
        << ", qy:" << initial_pose_.pose.pose.orientation.y
        << ", qz:" << initial_pose_.pose.pose.orientation.z
        << ", qw:" << initial_pose_.pose.pose.orientation.w);
    sub_.reset();

  }
};

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(PoseInitGeneratorNode)
